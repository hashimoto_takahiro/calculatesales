package jp.alhinc.hashimoto_takahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class BranchInfo {
	String code;
	String name;
    Long totalSales;
    
    public BranchInfo(String code, String name) {
        this.code = code;
        this.name = name;
        this.totalSales = 0L;
    }

    public BranchInfo add(Long sales) {
        BranchInfo branchInfo = new BranchInfo(this.code, this.name);
        branchInfo.totalSales = sales + this.totalSales;
        return branchInfo;
    }
}

public class CalculatSales {
	public static void main(String[] args) {

        Map<String, BranchInfo> branches = new HashMap<>();
        File branchLst = new File(args[0], "branch.lst");
        
        if(branchLst.exists() == false) {
            System.out.println("支店定義ファイルが存在しません");
            return;
        }

		BufferedReader br = null;
		try {
            br = new BufferedReader(new FileReader(branchLst));

			String line;
			while((line = br.readLine()) != null) {
				if((line.matches("^[0-9]{3},.*$")) == false) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
                    return;
                }
                String[] numName = line.split(",");
                branches.put(numName[0], new BranchInfo(numName[0], numName[1]));
			}
			
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
                    System.out.println("予期せぬエラーが発生しました");
                    return;
				}
			}
		}

        HashMap<String, Long> mNumSales = new HashMap<String, Long>();//map2支店番号、売り上げ

		File dir = new File(args[0]);
        File files[] = dir.listFiles();

        Arrays.sort(files);

        List<String> rcdFiles = new ArrayList<>();
        for (File file : files) {
			if (file.getName().matches("^[0-9]{8}.rcd$") == true) {
                rcdFiles.add(file.getName());
            }
        }


        int min = Integer.parseInt(rcdFiles.get(0).substring(1, 8));
        int max = Integer.parseInt(rcdFiles.get(rcdFiles.size() - 1).substring(1, 8));

        if (min + rcdFiles.size() - 1 != max) {
            System.out.println("売上ファイル名が連番になっていません");
            return;
        }

        for(String rcdFilename : rcdFiles) {
            //売り上げファイルが連番になっていない場合プログラムを終了する
            try {
                br = new BufferedReader(new FileReader(new File(args[0], rcdFilename)));
                String code = br.readLine();
                Long sales = Long.parseLong(br.readLine());
                
                //3行目がnullでなかった場合プログラムを終了する
                if(br.readLine() != null) {
                    System.out.println(rcdFilename + "のフォーマットが不正です");
                    return;
                }
                
                mNumSales.put(code, sales);
                
                if (branches.containsKey(code) == false) {
                    System.out.println(rcdFilename + "の支店コードが不正です");
                    return;
                }

                BranchInfo branchInfo = branches.get(code);
                BranchInfo summed = branchInfo.add(sales);
                
                //売り上げの合計金額が10桁より大きい場合プログラムを終了する
                if(summed.totalSales.toString().length() > 10) {
                    System.out.println("合計金額が10桁を超えました");
                    return;
                }

                branches.put(code, summed);
                    
            } catch(IOException e) {
                System.out.println("予期せぬエラーが発生しました");
                return;
            } finally {
                if(br != null) {
                    try {
                        br.close();
                    } catch(IOException e) {
                        System.out.println("予期せぬエラーが発生しました");
                        return;
                    }
                }
			}
        }
        
		//ファイルに書き出す
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(args[0], "branch.out")));
            for (BranchInfo branchInfo : branches.values()) {
                bw.write(branchInfo.code + "," + branchInfo.name + "," + branchInfo.totalSales);
                bw.newLine();
            }
			bw.close();
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
